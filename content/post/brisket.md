---
title: Brisket
date: 2021-08-12
---

## Priprema mesa

1. Pregledati meso i maknuti viskove pogotovo tvrdo salo koje se ne moze otopiti
2. malo skrojiti po potrebi da bude lijepog aerodinamicnog oblika
3. Namazati izvana sa senfom da se uhvati dry rub
3. Natrljati s dry rubom

## Dry Rub

1. Sol
2. Papar
3. Paprika slatka


## Smokanje

1. 3h na 120-125 stupnjeva i tek nakon 3h pospricati s mixom jabucnog octa i soka od jabuke
2. Sljedecih 2h treba ici s temperaturom 130 - 132 i spricati svakih sat vremena s mixom
3. Nakon otprilike 5h treba podici jos malo temperaturu na 135 - 138
4. Nakon 6h zamatamo u papir za pecenje poduplan minimalno i stavljamo na smoker na oko 140 stupnjeva
5. Drzimo zamotano u smokeru jos barem 2h, interna temperatura mesa mora biti na cca 85+ stupnjeva jer se u carry overy jos dokuha do zeljenih 90-93 stupnja
6. Nakon sto izvadimo trebamo jos barem 1h ostaviti da se hladi (temp se moze provjeriti s termmometrom nakon recimo 30-ak min i trebalo bi biti na 93)

![Brisket](/img/brisket.jpg)