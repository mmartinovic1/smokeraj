---
title: Rebarca
date: 2021-08-13
---

## Priprema mesa

1. Maknuti visak mesa s kraja rebara i iznad opne
2. Odrezati visak mesa s kraja rebara
3. Maknuti opnu s rebara

## Dry Rub

1. Sol
2. Papar
3. Paprika slatka
3. Chilly u prahu
4. Cesnjak u prahu
5. Luk u prahu


## Smokanje

1. 2h na 125-135 stupnjeva i svakih 1h pospricati s mixom jabucnog octa i soka od jabuke
2. Premazati pred kraj s bbq umakom
2. Kad temperatura mesa bude oko 73 stupnja zamatamo u foliju
4. Foliju malo naspricamo s mixom jabuke i jabucnog octa i namazemo s bbq umakom
5. Rebarca drzimo jos cca 1,5 - 2h dok interna temperatura ne bude oko 93 stupnja
6. Rebarca odmaramo 45 min do 1h

![Rebarca](/img/rebarca.jpg)