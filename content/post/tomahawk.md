---
title: Tomahawk
date: 2021-08-12
---

## Priprema mesa

1. Osusiti od krvi
2. Namazati malo sa suncokretovim uljem


## Dry Rub

1. Sol
2. Papar


## Priprema

1. Staviti na indirektnu vatru dok ne postigne 52 stupnja internal
2. Staviti na direktnu vatru po 1 - 2 min sa svake strane (ovisi da li zelimo rare/medium/well done)
3. Opciolano zalijevati s maslacem u kojem je par glavica cesnjaka i ruzmarin

![Brisket](/img/tomahawk.jpg)