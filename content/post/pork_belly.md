---
title: Pork belly cijeli
date: 2021-08-12
---

## Priprema mesa

1. Pregledati meso i maknuti viskove ako ih ima
2. Zarezati kozu oko 0,5 cm dijagonalno da dobijemo oblik dijamanta
3. Natrljati s dry rubom

## Dry Rub

1. Sol
2. Papar
3. Paprika slatka
3. Chilly u prahu
4. Cesnjak u prahu
5. Luk u prahu


## Smokanje

1. 2h na 125-135 stupnjeva i svakih 1h pospricati s mixom jabucnog octa i soka od jabuke
2. Premazati pred kraj s bbq umakom
2. Kad temperatura mesa bude oko 73 stupnja zamatamo u foliju
4. Foliju malo naspricamo s mixom jabuke i jabucnog octa i namazemo s bbq umakom
5. Belly drzimo jos cca 1h dok interna temperatura ne bude oko 93 stupnja
6. Belly odmaramo 45 min do 1h

![Pork belly](/img/belly.jpg)