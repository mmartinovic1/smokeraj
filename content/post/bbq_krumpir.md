---
title: BBQ krumpir
date: 2021-08-12
---

## Sastojci:

1. 0,5 - 1 kg krumpira
2. Zacini po zelji
 


## Priprema

1. Oguliti krumpir i cijelog ga prokuhati 6-7 min
2. Narezati na ploske jednake debljine
3. Poslagati ploske na resetku ili nabosti na raznjice
4. Peci cca 20-ak min

![BBQ krumpir](/img/bbq_krumpir.jpg)