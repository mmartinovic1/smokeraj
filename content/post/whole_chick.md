---
title: Cijelo pile (M.M)
date: 2021-08-12
---

## Priprema mesa

1. Pile ocistiti od sala, ostataka perja itd.
2. Pile raspoloviti po ledjnoj strani bez presijecanja prsa (napraviti kao leptir)
3. Pile malo premazati s uljem da se zacini bolje prime


## Dry Rub

1. Sol 
2. Papar
3. Paprika
4. Malo kurkume
5. Cesnjak u prahu
6. Luk u prahu


## Smokanje

1. Pile staviti na 125-130 stupnjeva i nakon 1h premazati s bbq umakom
2. Nakon 2h premazati opet bbq umakom
3. Ostaviti na smokeru prema potrebi dok interna temperatura na najdebljem dijelu prsa ne bude barem 85 stupnjeva


![Brisket](/img/chicken_smoked.jpg)