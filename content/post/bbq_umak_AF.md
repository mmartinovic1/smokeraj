---
title: BBQ umak (Aaron F.)
date: 2021-08-12
---


## Sastojci

1. Polovica manjeg luka ili trecina veceg 
2. 4 cesnja cesnjaka
3. 2 salice smedjeg secera
4. 1 salica jabucnog octa
5. 2 salice sos parajaza (domaci ili kupovna passata npr.)
6. 1 cajna zlica soli
7. 1 cajna zlica papra
8. 1 cajna zlica crvene paprike
9. 4 sprica Worcestershire umak

## Priprema

1. U posudu staviti ulja (suncokretovo) ili svinjske masti
2. Na sitne kockice narezati luk i ubaciti na srednju vatru da se dinsta
3. Nasjeckati cesnjak na sitno i ubaciti da se kratko dinsta zajedno s lukom
4. Nakon sto je luk malo omeksao ubaciti secer i mijesati na slabijoj do srednje jakon vatri dok se ne rastopi skroz
5. Ubaciti ocat
6. Ubaciti sos paradajz
7. Dodati sve zacine (Sol, papar, papriku i Worcestershire umak)
8. Kuhati jos 2 min da se sve poveze
9. Nakon sto se ohladi izmmiksati stapnim mixerom


Umak je pogodan za rebarca i piletinu

![BBQ umak Aaron F](/img/sauce.png)