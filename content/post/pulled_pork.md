---
title: Pulled pork texas style
date: 2021-08-12
---

## Priprema mesa

1. Lopaticu ocistiti od hrskavica, opni i viska sala
2. Odrezati sve komade koji 'strse' i tanke komade koji bi se mogli presusiti
3. Oblikovati malo meso da bude okruglasto
4. Meso premazati senfom ili uljem


## Dry Rub

1. Sol  
2. Papar
3. Crvena paprika

Omjer 70% soli 30% papra i samo mrvica paprike radi boje

## Smokanje i priprema

1. Meso smokati na 125-130 stupnjeva oko 3h
2. Nakon svakih 1h spricati s mixom jabucnog octa i jabucnog soka
3. Nakon 3h smokanja izmjeriti internu temperaturu - trebala bi biti oko 73
4. Zamotati u aluminijsku foliju koju prethodno malo naspricamo s mixom jabucnog octa i soka
5. Zamotano drzimo na 135 stupnjeva jos 2h
6. Kad interna temperatura bude oko 93 spremno je za odmaranje
7. Odmaramo barem 30 - 45 min

![Pulled pork](/img/pulled_pork.jpg)